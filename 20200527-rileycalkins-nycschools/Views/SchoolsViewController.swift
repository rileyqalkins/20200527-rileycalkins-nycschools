//
//  SchoolsViewController.swift
//  20200527-rileycalkins-nycschools
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import UIKit

class SchoolsViewController: UIViewController {
    
    @IBOutlet private weak var schoolsTableView: UITableView!
    
    let viewModal = SchoolsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        schoolsTableView.delegate = self
        schoolsTableView.dataSource = self
        
        viewModal.reloadData { [weak self] in
            guard let self = self else { return }
            self.schoolsTableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let schoolsDetailViewController = segue.destination as? SchoolsDetailViewController,
            let item = schoolsTableView.indexPathForSelectedRow?.row,
            let school = viewModal.school(at: item) else { return }
        
        schoolsDetailViewController.schoolVM = SchoolDetailViewModel(school: school)
    }
    
}

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModal.totalSchools
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolNameCell", for: indexPath) as! SchoolCell
        
        if let school = viewModal.school(at: indexPath.row) {
            cell.configure(with: school)
        }
        
        return cell
    }
}

