//
//  SchoolsDetailViewModel.swift
//  20200527-rileycalkins-nycschools
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

class SchoolDetailViewModel {
    
    private var school: School
    
    init(school: School) {
        self.school = school
    }
    
    var schoolName: String {
        return school.schoolName ?? ""
    }
    
    var satTakersCount: String {
        return school.satTakersCount ?? "0"
    }
    
    var criticalReadingAvgScore: String {
        return school.criticalReadingAvgScore ?? "0"
    }
    
    var mathAvgScore: String {
        return school.mathAvgScore ?? "0"
    }
    
    
    var writingAvgScore: String {
        return school.writingAvgScore ?? "0"
    }
    
    var satTotalScore: String {
        let writing = Int(writingAvgScore) ?? 0
        let math = Int(mathAvgScore) ?? 0
        let criticalReading = Int(criticalReadingAvgScore) ?? 0
        
        let total = "\(writing + math + criticalReading)"
        return total
    }
}

