//
//  SchoolCell.swift
//  20200527-rileycalkins-nycschools
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {
    
    @IBOutlet private weak var lblSchoolName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with school: School) {
        lblSchoolName.text = school.schoolName
    }
    
    override func prepareForReuse() {
        lblSchoolName.text = ""
    }
}
