//
//  SchoolsDetailViewController.swift
//  20200527-rileycalkins-nycschools
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import Foundation

import UIKit

class SchoolsDetailViewController: UIViewController {
    
    @IBOutlet private weak var lblSchoolName: UILabel!
    @IBOutlet private weak var lblSATTakers: UILabel!
    @IBOutlet private weak var lblCriticalReadingAverage: UILabel!
    @IBOutlet private weak var lblMathAverage: UILabel!
    @IBOutlet private weak var lblWritingAverage: UILabel!
    @IBOutlet weak var lblSatTotal: UILabel!
    
    
    var schoolVM: SchoolDetailViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLabels()
    }
    
    func configureLabels() {
        guard let schoolVM = schoolVM else { return }
        lblSchoolName.text = schoolVM.schoolName
        lblSATTakers.text = schoolVM.satTakersCount
        lblCriticalReadingAverage.text = "\(schoolVM.criticalReadingAvgScore) /800"
        lblMathAverage.text = "\(schoolVM.mathAvgScore) /800"
        lblWritingAverage.text = "\(schoolVM.writingAvgScore) /800"
        lblSatTotal.text = "\(schoolVM.satTotalScore) /2400"
    }
}
