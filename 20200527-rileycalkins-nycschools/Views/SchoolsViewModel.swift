//
//  SchoolsViewModel.swift
//  20200527-rileycalkins-nycschools
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import UIKit

class SchoolsViewModel: NSObject {
    
    private let apiManager = APIManager()
    internal var schools: [School] = []
    
    var totalSchools: Int {
        return schools.count
    }
    
    func reloadData(with completion: @escaping () -> Void) {
        apiManager.downloadSchools { [weak self] in
            guard let self = self else { return }
            self.schools = $0
            completion()
        }
    }
    
    func school(at index: Int) -> School? {
        guard (0..<schools.count).contains(index) else { return nil }
        return schools[index]
    }
}

