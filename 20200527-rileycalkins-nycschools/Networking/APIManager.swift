//
//  APIManager.swift
//  20200527-rileycalkins-nycschools
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import Foundation

private enum URLs {
    static let schoolsURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

class APIManager {
    
    func downloadSchools(with completion: @escaping ([School]) -> Void){
        guard let url = URL(string: URLs.schoolsURL) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // Check for errors
            if let error = error {
                print("Error: \(error.localizedDescription)")
                return
            }
            
            // Parse
            if let data = data {
                
                do {
                    let schools = try JSONDecoder().decode([School].self, from: data)
                    DispatchQueue.main.async {
                        completion(schools)
                    }
                } catch {
                    print("Error: \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        completion([])
                    }
                }
            }
            }.resume()
    }
}
