//
//  SchoolDetailViewModelSpec.swift
//  20200527-rileycalkins-nycschoolsTests
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import XCTest

class SchoolDetailViewModelSpec: XCTestCase {
    
    let blankSchoolData = "{}".data(using: .utf8)
    var blankSchool: School?
    
    let validSchoolData = "{\"dbn\":\"01M292\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"sat_total_score\":\"1122\"}".data(using: .utf8)
    var validSchool: School?
    
    override func setUp() {
        guard let blankSchoolData = blankSchoolData,
            let validSchoolData = validSchoolData else {
                
                XCTFail("JSON Data construction failed")
                return
        }
        
        do {
            blankSchool = try JSONDecoder().decode(School.self, from: blankSchoolData)
            validSchool = try JSONDecoder().decode(School.self, from: validSchoolData)
        } catch {
            XCTFail("JSON Parsing failed")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBlankSchoolName() {
        guard let blankSchool = blankSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: blankSchool)
        
        XCTAssertEqual(viewModel.schoolName, "")
    }
    
    func testBlankCriticalReadingAvgScore() {
        guard let blankSchool = blankSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: blankSchool)
        
        XCTAssertEqual(viewModel.criticalReadingAvgScore, "0")
    }
    
    func testBlankSatTakersCount() {
        guard let blankSchool = blankSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: blankSchool)
        
        XCTAssertEqual(viewModel.satTakersCount, "0")
    }
    
    func testBlankMathAvgScore() {
        guard let blankSchool = blankSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: blankSchool)
        
        XCTAssertEqual(viewModel.mathAvgScore, "0")
    }
    
    func testBlankWritingAvgScore() {
        guard let blankSchool = blankSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: blankSchool)
        
        XCTAssertEqual(viewModel.writingAvgScore, "0")
    }
    
    
    func testValidSchoolName() {
        guard let validSchool = validSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: validSchool)
        
        XCTAssertEqual(viewModel.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
    }
    
    func testValidCriticalReadingAvgScore() {
        guard let validSchool = validSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: validSchool)
        
        XCTAssertEqual(viewModel.criticalReadingAvgScore, "355")
    }
    
    func testValidSatTakersCount() {
        guard let validSchool = validSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: validSchool)
        
        XCTAssertEqual(viewModel.satTakersCount, "29")
    }
    
    func testValidMathAvgScore() {
        guard let validSchool = validSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: validSchool)
        
        XCTAssertEqual(viewModel.mathAvgScore, "404")
    }
    
    func testValidWritingAvgScore() {
        guard let validSchool = validSchool else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolDetailViewModel(school: validSchool)
        
        XCTAssertEqual(viewModel.writingAvgScore, "363")
    }
    
    func testValidSatTotalScore() {
        guard let validSchool = validSchool else {
            XCTFail()
            return
        }
        let viewModel = SchoolDetailViewModel(school: validSchool)
        
        XCTAssertEqual(viewModel.satTotalScore, "1122")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
