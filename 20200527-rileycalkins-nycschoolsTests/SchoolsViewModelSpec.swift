//
//  SchoolsViewModelSpec.swift
//  20200527-rileycalkins-nycschoolsTests
//
//  Created by Riley Calkins on 5/27/20.
//  Copyright © 2020 Riley. All rights reserved.
//

import XCTest

class SchoolsViewModelSpec: XCTestCase {
    
    let validSchoolsJSONData = "[{\"dbn\":\"01M292\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\"},{\"dbn\":\"01M448\",\"school_name\":\"UNIVERSITY NEIGHBORHOOD HIGH SCHOOL\",\"num_of_sat_test_takers\":\"91\",\"sat_critical_reading_avg_score\":\"383\",\"sat_math_avg_score\":\"423\",\"sat_writing_avg_score\":\"366\",\"sat_total_score\":\"1172\"}]".data(using: .utf8)
    var validSchools: [School]?
    
    let blankSchoolJSONData = "[]".data(using: .utf8)
    var blankSchools: [School]?
    
    override func setUp() {
        guard let blankSchoolJSONData = blankSchoolJSONData,
            let validSchoolsJSONData = validSchoolsJSONData else {
                XCTFail("JSON Data construction failed")
                return
        }
        
        do {
            blankSchools = try JSONDecoder().decode([School].self, from: blankSchoolJSONData)
            validSchools = try JSONDecoder().decode([School].self, from: validSchoolsJSONData)
        } catch {
            XCTFail("JSON Parsing failed")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBlankSchoolCount() {
        guard let blankSchools = blankSchools else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolsViewModel()
        viewModel.schools = blankSchools
        
        XCTAssertEqual(viewModel.totalSchools, 0)
    }
    
    func testBlankSchoolAt() {
        guard let blankSchools = blankSchools else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolsViewModel()
        viewModel.schools = blankSchools
        
        XCTAssertNil(viewModel.school(at: 0))
        XCTAssertNil(viewModel.school(at: 5))
    }
    
    func testValidSchoolCount() {
        guard let validSchools = validSchools else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolsViewModel()
        viewModel.schools = validSchools
        
        XCTAssertEqual(viewModel.totalSchools, 2)
    }
    
    func testValidSchoolAt() {
        guard let validSchools = validSchools else {
            XCTFail()
            return
        }
        
        let viewModel = SchoolsViewModel()
        viewModel.schools = validSchools
        
        XCTAssertNotNil(viewModel.school(at: 0))
        XCTAssertNotNil(viewModel.school(at: 1))
        XCTAssertNil(viewModel.school(at: 2))
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
